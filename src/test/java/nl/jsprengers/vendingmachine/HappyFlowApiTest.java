package nl.jsprengers.vendingmachine;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Here we will add test scenarios for the so-called happy flows, that is the normal operation of the machine.
 * Think of all the different input and outputs:
 * - Insert one or more different coins
 * - Choose any of the three beverages
 * - Receive change (or not)
 */
public class HappyFlowApiTest {

    /**
     * This is what a typical test scenario looks like.
     * The JUnit mechanism runs every test case annotated with @Test. If they don't throw an exception, it is assumed that the scenario passed.
     * Note that since this method doesn't handle all the exceptions explicitly in a try/catch block, it has to declare them in a throws clause.
     * That's OK for now, because we don't expect any exceptions, and if they should occur there's a mistake in our test and we have to fix it anyway.
     */
    @DisplayName("Buy a can of Coke with one 25 c. coin returns no change")
    @Test
    void scenario_one() throws SoldOutException, InsufficientChangeException, InsufficientAmountInserted {
        //GIVEN I have a full vending machine
        VendingMachine machine = new AmericanSodaMachine();

        //WHEN I select a Coke and insert a quarter and collect the item
        machine.selectItemAndGetPrice(Item.COKE);
        machine.insertCoin(AmericanCoin.QUARTER_25);
        var output = machine.collectItemAndChange();

        //THEN I have received a COKE and no change
        var itemReceived = output.item();
        //AssertJ is a utility to ensure that a certain value returned by the class under test meets expectations.
        // The only method you need to remember is assertThat(..) and it has dedicated methods for numbers, lists, dates etc.

        Assertions.assertThat(itemReceived).isSameAs(Item.COKE);
        Assertions.assertThat(output.refund()).isEmpty();
    }


    @DisplayName("Free-text description of test scenario two.")
    @Test
    void scenario_two(){

    }
}
