package nl.jsprengers.vendingmachine;

import java.util.List;

public interface VendingMachine {

    /**
     * The first step in the user flow: pick your desired beverage
     * @param item the chosen item
     * @return the price in cents
     * @throws SoldOutException when the item is no longer in stock
     */
    int selectItemAndGetPrice(Item item) throws SoldOutException;

    /**
     * Repeat this step to insert coins in order to reach the required price for the item you selected
     * @param coin a valid coin
     */
    void insertCoin(AmericanCoin coin);

    /**
     * Cancel the order and get back the coins you already put in.
     * @return A list of coins, equal to the amount you put in
     * @throws InsufficientChangeException when the machine has not enough change available
     */
    List<AmericanCoin> refund() throws InsufficientChangeException;

    /**
     * Collect the item.
     * @return the chosen beverage, plus any change
     * @throws InsufficientChangeException when the machine cannot give you back the correct change
     * @throws InsufficientAmountInserted when you have not inserted enough coins
     */
    Output collectItemAndChange() throws InsufficientChangeException, InsufficientAmountInserted;

}
