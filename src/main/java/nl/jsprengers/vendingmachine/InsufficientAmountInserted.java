package nl.jsprengers.vendingmachine;

public class InsufficientAmountInserted extends Exception {

    private final long remaining;

    public InsufficientAmountInserted(String message, long remaining) {
        super(message);
        this.remaining = remaining;
    }

    public long getRemaining() {
        return remaining;
    }

}
