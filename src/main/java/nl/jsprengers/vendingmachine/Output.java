package nl.jsprengers.vendingmachine;

import java.util.List;

public record Output(Item item, List<AmericanCoin> refund) {

}
