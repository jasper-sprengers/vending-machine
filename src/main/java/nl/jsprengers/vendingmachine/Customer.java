package nl.jsprengers.vendingmachine;

public class Customer {

    public static void main(String[] args) {
        var sodaMachine = VendingMachineFactory.createAmericanSodaMachine();
        try {
            var price = sodaMachine.selectItemAndGetPrice(Item.COKE);
            System.out.printf("A %s? That will be %s cents.\n", Item.COKE, price);
            sodaMachine.insertCoin(AmericanCoin.QUARTER_25);
            var output = sodaMachine.collectItemAndChange();
            int coinsReturned = output.refund().size();
            var item = output.item().getName();
            System.out.printf("Enjoy your %s!\n", item);
            System.out.printf("%d coins returned ", coinsReturned);
        } catch (SoldOutException e) {
            System.out.println("Oops, no more Coke");
        } catch (InsufficientChangeException e) {
            System.out.println("Oops, cannot return suitable change.");
        } catch (InsufficientAmountInserted insufficientAmountInserted) {
            System.out.printf("Oops, you're %d cents short%n", insufficientAmountInserted.getRemaining());
        }
    }

}
